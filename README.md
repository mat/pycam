# PyCam
PyCam is a Python script to be deployed on a Raspberry Pi for reading a Raspberry Pi camera and stream a image to a URL.
The image is embedded into a html website or can be viewed directly.
The image stream can be read for example by FritzBox Fon integrated into a door bell system.

Codeberg CI is focusing on code quaility.  
[![status-badge](https://ci.codeberg.org/api/badges/mat/pycam/status.svg)](https://ci.codeberg.org/mat/pycam)


## How to deploy
Prepare the Raspberry Pi for accessing picam from Docker. 
`sudo nano /etc/udev/rules.d/99-camera.rules` and insert the following content `SUBSYSTEM=="vchiq",MODE="0666"`.

Activate the camera interface:
```commandline
sudo raspi-config
-> 3 Interface Options    Configure connections to peripherals
-> I1 Legacy Camera Enable/disable legacy camera support
-> yes
```

# Clone the repository to you Raspberry Pi:  
`git clone https://codeberg.org/mat/pycam.git`

```
sudo apt install python3-virtualenv
virtualenv ./venv_pycam
source venv_pycam/bin/activate
pip install -r requirements.txt
cd pycam
python3 pyapp.py
```

Navigate to the devices IP address and the port 5000, e.g. `http://192.168.178.10:5000/`, to open the welcome site 
and open `http://192.168.178.10:5000/image` to receive an image. 

#### Install Docker and Docker-compose (not working for now on Raspberry Pi)
curl -fsSL https://get.Docker.com -o get-Docker.sh
sudo sh get-Docker.sh

sudo usermod -aG docker $USER
newgrp docker

sudo apt install docker-compose


Clone the repository to you Raspberry Pi:  
`git clone https://codeberg.org/mat/pycam.git`

Build the Dockerfile with:  
`docker build -t pycam .`

Start the application from your directory:  
`docker-compose up`

