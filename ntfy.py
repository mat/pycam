from requests import post

def publish_text_to_ntfy(text_string: str) -> str:
    server_url = "https://ntfy.sh/mnethome-py"
    r = post(url=server_url, data=text_string.encode(encoding='utf-8'))
    return r.status_code

def publish_text_image_to_ntfy(text_string: str) -> str:
    server_url = "https://ntfy.sh/mnethome-py"
    image_stream = "http://192.168.178.50:5000/image"
    r = post(url=server_url,
             data="image".encode('utf-8'),
             headers={
                 "Click": image_stream,
                 "Attach": image_stream,
                 #"Actions": "http, Open door, https://api.nest.com/open/yAxkasd, clear=true",
    })
    return r.status_code
