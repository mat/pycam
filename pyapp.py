from flask import Flask, Response, send_file
from io import BytesIO
from time import sleep
from picamera import PiCamera
from PIL import Image
from ntfy import publish_text_image_to_ntfy

app = Flask(__name__)

@app.route("/")
def index():
    return "Hello World, Pycam is up and running!"

def read_picam():
    stream = BytesIO()
    camera = PiCamera()
    camera.start_preview()
    sleep(2)
    camera.capture(stream, format='jpeg')
    stream.seek(0)
    image = Image.open(stream)
    camera.close()
    return image

@app.route("/image")
def image():
    img = read_picam()
    #publish_text_image_to_ntfy("")
    imgByteArr = BytesIO()
    img.save(imgByteArr, format='jpeg')
    imgByteArr.seek(0)
    return send_file(imgByteArr, mimetype="image/jpeg")

@app.route("/action_image")
def action_image():
    publish_text_image_to_ntfy("take image")
    return "Image has been recorded and send to Ntfy"

if(__name__ == "__main__"):
    app.run(host='0.0.0.0')
