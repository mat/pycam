import json
def json_file_writer(json_metadata: dict, file_name: str) -> bool:
    """
    Writing json structured data into a file.
    Parameters
    ----------
    json_metadata: json structured meta information
    file_name: file name as string

    Returns
    -------
    True if file is written successfully
    """
    with open(file_name, "w") as json_file:
        json.dump(json_metadata, json_file)
    return True


def json_file_reader(file_name: str) -> dict:
    """
    Reading a json file.
    Parameters
    ----------
    file_name: file name as string

    Returns
    -------
    Return dict of json data
    """
    with open(file_name) as json_file:
        data = json.load(json_file)
    return data