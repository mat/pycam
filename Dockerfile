FROM python:3.9
ADD . /pycam
WORKDIR /pycam

RUN apt-get update && apt-get install -y

RUN apt install ffmpeg libsm6 libxext6  -y
RUN apt install libgl1 -y
RUN apt install python3-opencv -y

#RUN pip install -r requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install --upgrade pip setuptools wheel
RUN pip install Flask==2.2.2 Pillow==9.3.0 picamera

CMD python pyapp.py